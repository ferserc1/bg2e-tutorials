class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());

        bg.base.Loader.Load(this.gl, "resources/scene.vitscnj")
            .then((sceneData) => {
                this._camera = sceneData.cameraNode.camera;
                this._sceneRoot = sceneData.sceneRoot;

                sceneData.cameraNode.addComponent(new bg.manipulation.OrbitCameraController());

                this.postReshape();
                this.postRedisplay();
            });
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);

        this._inputVisitor = new bg.scene.InputVisitor();
    }
    
    frame(delta) {
        if (this._sceneRoot) {
            this._renderer.frame(this._sceneRoot, delta);
        }
    }

    display() {
        if (this._sceneRoot && this._camera) {
            this._renderer.display(this._sceneRoot, this._camera);
        }
    }
    
    reshape(width,height) {
        if (this._camera) {
            this._camera.viewport = new bg.Viewport(0,0,width,height);
            this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
        }
    }

    keyDown(evt) { if (this._sceneRoot) this._inputVisitor.keyDown(this._sceneRoot, evt); }
    keyUp(evt) { if (this._sceneRoot) this._inputVisitor.keyUp(this._sceneRoot, evt); }
    mouseUp(evt) { if (this._sceneRoot) this._inputVisitor.mouseUp(this._sceneRoot, evt); }
    mouseDown(evt) { if (this._sceneRoot) this._inputVisitor.mouseDown(this._sceneRoot, evt); }
    mouseMove(evt) { if (this._sceneRoot) this._inputVisitor.mouseMove(this._sceneRoot, evt); }
    mouseOut(evt) { if (this._sceneRoot) this._inputVisitor.mouseOut(this._sceneRoot, evt); }
    mouseDrag(evt) { if (this._sceneRoot) this._inputVisitor.mouseDrag(this._sceneRoot, evt); }
    mouseWheel(evt) { if (this._sceneRoot) this._inputVisitor.mouseWheel(this._sceneRoot, evt); }
    touchStart(evt) { if (this._sceneRoot) this._inputVisitor.touchStart(this._sceneRoot, evt); }
    touchMove(evt) { if (this._sceneRoot) this._inputVisitor.touchMove(this._sceneRoot, evt); }
    touchEnd(evt) { if (this._sceneRoot) this._inputVisitor.touchEnd(this._sceneRoot, evt); }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}