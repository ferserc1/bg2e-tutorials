class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());

        bg.base.Loader.Load(this.gl, "resources/scene.vitscnj")
            .then((sceneData) => {
                this._camera = sceneData.cameraNode.camera;
                this._sceneRoot = sceneData.sceneRoot;
                this.postReshape();
                this.postRedisplay();
            });
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        if (this._sceneRoot) {
            this._renderer.frame(this._sceneRoot, delta);
        }
    }

    display() {
        if (this._sceneRoot && this._camera) {
            this._renderer.display(this._sceneRoot, this._camera);
        }
    }
    
    reshape(width,height) {
        if (this._camera) {
            this._camera.viewport = new bg.Viewport(0,0,width,height);
            this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
        }
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}