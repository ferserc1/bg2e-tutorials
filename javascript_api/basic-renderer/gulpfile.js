const   gulp = require('gulp'),
        sourcemaps = require('gulp-sourcemaps'),
        traceur = require('gulp-traceur'),
        concat = require('gulp-concat'),
        connect = require('gulp-connect'),
		replace = require('gulp-replace'),
		sort = require('gulp-sort'),
		minify = require('gulp-minify'),
		fs = require('fs'),
		path = require('path');

var config = {
	outDir:'build',
    serverPort: 8888,
    sourceFiles:[
        "index.js"
    ],
    watchFiles:[
        "index.js",
        "index.html"
    ]
}

gulp.task("webserver", function() {
	connect.server({
		livereload: true,
		port: config.serverPort,
		root: config.outDir
	});
});

gulp.task("copy", function() {
    gulp.src("bower_components/traceur/traceur.min.js")
        .pipe(gulp.dest(config.outDir + '/js'));

    gulp.src("bower_components/bg2e-js/js/bg2e.js")
        .pipe(gulp.dest(config.outDir + '/js'));
    
    gulp.src("*.html")
        .pipe(gulp.dest(config.outDir));
});

gulp.task("compile", function() {
    gulp.src(config.sourceFiles)
        .pipe(sort())
        .pipe(traceur())
        .pipe(concat('bg2e-sample.js'))
        .pipe(gulp.dest(config.outDir + '/js'));
});

gulp.task("watch", function() {
    gulp.watch(config.watchFiles,["compile","copy"]);
});

gulp.task("build",["compile","copy"]);
gulp.task("default",["build","webserver","watch"]);
