class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
    }

    createObjects() {
        bg.base.Loader.Load(this.gl, "resources/cube.obj")
            .then((node) => {
                this._sceneRoot.addChild(node);
            })
            .catch((err) => {
                alert(err);
            });

        let floorNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(floorNode);
        floorNode.addComponent(bg.scene.PrimitiveFactory.Plane(this.gl,10));
        floorNode.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-1,0)));
    }

    createLight() {
        let lightNode = new bg.scene.Node(this.gl);
        this._sceneRoot.addChild(lightNode);

        lightNode.addComponent(new bg.scene.Light(new bg.base.Light(this.gl)));
        lightNode.addComponent(new bg.scene.Transform(
            new bg.Matrix4.Identity()
                .translate(0,0,5)
                .rotate(bg.Math.degreesToRadians(15),0,1,0)
                .rotate(bg.Math.degreesToRadians(55),-1,0,0)
                .translate(0,1.4,3)
        ));
    }

    createCamera() {
        let cameraNode = new bg.scene.Node(this.gl,"Main camera");
        this._sceneRoot.addChild(cameraNode);

        this._camera = new bg.scene.Camera();
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform(
            bg.Matrix4.Translation(0.2,0,0)
                .rotate(bg.Math.degreesToRadians(-25),0,1,0)
                .rotate(bg.Math.degreesToRadians(22.5),-1,0,0)
                .translate(0,0,3)
        ));
    }

    buildScene() {
        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.Bg2LoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.OBJLoaderPlugin());

        this._sceneRoot = new bg.scene.Node(this.gl,"Scene Root");

        this.createObjects();
        this.createLight();
        this.createCamera();
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        this.buildScene();

        this._renderer = bg.render.Renderer.Create(
            this.gl,
            bg.render.RenderPath.FORWARD
        );
        this._renderer.clearColor = new bg.Color(0.2,0.4,0.7,1);
    }
    
    frame(delta) {
        this._renderer.frame(this._sceneRoot, delta);
    }

    display() {
        this._renderer.display(this._sceneRoot, this._camera);
    }
    
    reshape(width,height) {
        this._camera.viewport = new bg.Viewport(0,0,width,height);
        this._camera.projection.perspective(60,this._camera.viewport.aspectRatio,0.1,100);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}