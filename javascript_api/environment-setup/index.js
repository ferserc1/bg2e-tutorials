(function() {

    window.bg2eSample = window.bg2eSample || {};

    class TestClass {
        constructor(greeting) {
            alert(greeting);
        }
    }
    
    bg2eSample.TestClass = TestClass;

})();