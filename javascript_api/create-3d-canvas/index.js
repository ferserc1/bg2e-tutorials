class MyWindowController extends bg.app.WindowController {
    constructor() {
        super();
        this.clearColor = [0,0.4,0.9,1];
    }
    
    init() {
        this.gl.clearColor(...this.clearColor);
    }
    
    display() {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }
    
    reshape(width,height) {
        this.gl.viewport(0,0,width,height);
    }
}

function load(canvas) {
    let ctrl = new MyWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = canvas;
    mainLoop.run(ctrl);
}