
#include <iostream>

#include <bg/bg2e.hpp>

class ClassA : public bg::base::ReferencedPointer {
public:
	ClassA() {
		std::cout << "ClassA constructor" << std::endl;
	}
	
	virtual void saySomething() const {
		std::cout << "Hello from ClassA" << std::endl;
	}

protected:
	virtual ~ClassA() {
		std::cout << "ClassA destructor" << std::endl;
	}
};


void example1() {
	std::cout << "Example 1" << std::endl;
	std::cout << "About to create ClassA" << std::endl;
	{
		bg::ptr<ClassA> classA = new ClassA();
		classA->saySomething();
	}
	
	std::cout << "ClassA destroyed" << std::endl;
}

void example2() {
	std::cout << "Example 2" << std::endl;
	bg::ptr<ClassA> classA = new ClassA();
	{
		bg::ptr<ClassA> classA1 = classA.getPtr();
		classA1->saySomething();
		std::cout << "classA1 references: " << classA1->getReferences() << std::endl;
		std::cout << "classA references: " << classA->getReferences() << std::endl;
	}
	
	std::cout << "classA references: " << classA->getReferences() << std::endl;
}

ClassA * factoryMethod() {
	bg::ptr<ClassA> result = new ClassA();
	
	// Do something with result
	
	return result.release();
}

void example3() {
	std::cout << "Example 3: factory method" << std::endl;
	bg::ptr<ClassA> result = factoryMethod();
	result->saySomething();
}

class Node : public bg::base::ReferencedPointer {
public:
	Node(const std::string & name) :_name{ name } {
		std::cout << "Constructor of " << name << std::endl;
	}
	
	inline void addChild(Node * n) {
		_children.push_back(n);
		n->_parent = this;
	}
	
protected:
	virtual ~Node() {
		std::cout << "Destructor of " << _name << std::endl;
	}
	
	const std::string _name;
	std::vector<bg::ptr<Node>> _children;
	Node * _parent;
};

void example4() {
	std::cout << "Cross references example" << std::endl;
	bg::ptr<Node> root = new Node("root");
	
	root->addChild(new Node("child 1"));
	root->addChild(new Node("child 2"));
}

int main(int argc, char ** argv) {
	example4();
	return 0;
}
