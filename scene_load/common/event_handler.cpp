
#include <event_handler.hpp>

MainEventHandler::MainEventHandler()
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("No such suitable rendering engine.");
	}
}

void MainEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());

	bg::db::NodeLoader::RegisterPlugin(new bg::db::plugin::ReadScene());
	bg::db::DrawableLoader::RegisterPlugin(new bg::db::plugin::ReadDrawableBg2());

	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);
	_inputVisitor = new bg::scene::InputVisitor();

	_sceneRoot = bg::db::loadScene(context(), bg::system::Path::ExecDir().pathAddingComponent("demo/scene.vitscnj"));

	bg::ptr<bg::scene::FindComponentVisitor<bg::scene::Camera>> findCameraVisitor = new bg::scene::FindComponentVisitor<bg::scene::Camera>();
	_sceneRoot->accept(findCameraVisitor.getPtr());

	for (auto cam : findCameraVisitor->result()) {
		_camera = cam;
		if (cam->node()->component<bg::scene::Transform>() == nullptr) {
			// No transform found in the camera component
		}
		if (cam->node()->component<bg::manipulation::OrbitNodeController>() == nullptr) {
			// No camera controller found in camera component
		}
	}
}

void MainEventHandler::willDestoryContext() {
	
}

void MainEventHandler::destroy() {
	
}

void MainEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0, 0, w, h));
}

void MainEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MainEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MainEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::charPress(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->charPress(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::onMemoryWarning() {
}

void MainEventHandler::touchStart(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchMove(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::sensorEvent(const bg::base::SensorEvent & evt) {
	_inputVisitor->sensorEvent(_sceneRoot.getPtr(), evt);
}
