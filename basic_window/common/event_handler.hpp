
#ifndef _event_handler_hpp_
#define _event_handler_hpp_

#include <bg/bg2e.hpp>

class MainEventHandler : public bg::base::EventHandler {
public:
	MainEventHandler();
	
	void willCreateContext();
	
	void initGL();
	void reshape(int,int);
	void frame(float);
	void draw();
	void keyUp(const bg::base::KeyboardEvent &);

protected:
	virtual ~MainEventHandler();
	
	bg::ptr<bg::base::Pipeline> _pipeline;
	bg::math::Color _clearColor;
	bg::math::Color _inc;
};

#endif
