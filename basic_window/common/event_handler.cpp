
#include <event_handler.hpp>

MainEventHandler::MainEventHandler()
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if(bg::engine::DirectX11::Supported()) {
		bg::Engine::Init(new bg::engine::DirectX11());
	}
	else if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("No such suitable rendering engine.");
	}
}

void MainEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	
	_pipeline = new bg::base::Pipeline(context());
	bg::base::Pipeline::SetCurrent(_pipeline.getPtr());
	
	_clearColor = bg::math::Color::White();
	_inc = bg::math::Color(0.1f, 0.05f, 0.07f, 0.0f);
	_pipeline->setClearColor(_clearColor);
	_pipeline->setBuffersToClear(bg::base::kColorDepth);
}

void MainEventHandler::reshape(int w, int h) {
	_pipeline->setViewport(bg::math::Viewport(0, 0, w, h));
}

void MainEventHandler::frame(float delta) {
	_clearColor = _clearColor + _inc * delta * 3.0f;
	_clearColor.clamp(0.0f, 1.0f);
	if (_clearColor == bg::math::Color::White() || _clearColor == bg::math::Color::Black()) {
		_inc = _inc * -1.0f;
	}
	_pipeline->setClearColor(_clearColor);
}

void MainEventHandler::draw() {
	_pipeline->clearBuffers();
	context()->swapBuffers();
}

void MainEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	if (evt.keyboard().key() == bg::base::Keyboard::kKeyEsc) {
		bg::wnd::MainLoop::Get()->quit(0);
	}
}
