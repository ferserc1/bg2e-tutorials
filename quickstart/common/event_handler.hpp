
#ifndef _event_handler_hpp_
#define _event_handler_hpp_

#include <bg/bg2e.hpp>

class MainEventHandler : public bg::base::EventHandler {
public:
	MainEventHandler();
	
	void willCreateContext();

protected:
	virtual ~MainEventHandler();
};

#endif
