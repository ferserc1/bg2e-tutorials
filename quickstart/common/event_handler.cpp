
#include <event_handler.hpp>

MainEventHandler::MainEventHandler()
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("Your system is not supported.");
	}
}
