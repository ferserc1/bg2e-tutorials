
#include <event_handler.hpp>

#include <iostream>

int main(int argc, char ** argv) {
	{
		bg::ptr<bg::wnd::Window> window = bg::wnd::Window::New();
		
		if (!window.valid()) {
			std::cerr << "Platform not supported" << std::endl;
			return -1;
		}
		
		window->setRect(bg::math::Rect(50,50,800,600));
		window->setTitle("Quickstart tutorial");
		window->setEventHandler(new MainEventHandler());
		window->create();
		
		bg::wnd::MainLoop::Get()->setWindow(window.getPtr());
	}
	return bg::wnd::MainLoop::Get()->run();
}
