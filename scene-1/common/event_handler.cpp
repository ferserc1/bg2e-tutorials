
#include <event_handler.hpp>

MainEventHandler::MainEventHandler()
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("No such suitable rendering engine.");
	}
}

void MainEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	{
		using namespace bg::scene;
		using namespace bg::math;
		_sceneRoot = new Node(context());

		Node * cameraNode = new Node(context(), "sceneRoot");
		_sceneRoot->addChild(cameraNode);
		_camera = new Camera();
		cameraNode->addComponent(_camera.getPtr());
		Transform * trx = new Transform();
		trx->matrix()
			.rotate(trigonometry::degreesToRadians(45.0f), 0.0f, 1.0f, 0.0f)
			.rotate(trigonometry::degreesToRadians(22.5), -1.0f, 0.0f, 0.0f)
			.translate(0.0f, 0.0f, Scalar(5,distance::meter));
		OpticalProjectionStrategy * projection = new OpticalProjectionStrategy;
		_camera->setProjectionStrategy(projection);
		projection->setFocalLength(35.0f);
		cameraNode->addComponent(trx);

		Node * lightNode = new Node(context());
		_sceneRoot->addChild(lightNode);
		trx = new Transform();
		trx->matrix()
			.rotate(trigonometry::degreesToRadians(95.0f), -1.0f, 0.0f, 0.0f)
			.rotate(trigonometry::degreesToRadians(45.0f), 0.0f, 1.0f, 0.0f);
		lightNode->addComponent(trx);
		lightNode->addComponent(new bg::scene::Light);

		PrimitiveFactory factory(context());
		Node * floor = new Node(context());
		_sceneRoot->addChild(floor);
		floor->addComponent(factory.plane(Scalar(5,distance::meter)));
		trx = new Transform();
		trx->matrix()
			.translate(0.0f, Scalar(-55,distance::cm), 0.0f);
		floor->addComponent(trx);

		Node * cube = new Node(context());
		trx = new Transform();
		trx->matrix()
			.rotate(trigonometry::degreesToRadians(22.5f), 0.0f, 1.0f, 0.0f);
		cube->addComponent(trx);
		_sceneRoot->addChild(cube);
		cube->addComponent(factory.cube(Scalar(1,distance::meter)));
		
		_camera->setFocus(Scalar(5,distance::meter));
	}
}

void MainEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0, 0, w, h));
}

void MainEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}
