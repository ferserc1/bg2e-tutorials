
#ifndef _event_handler_hpp_
#define _event_handler_hpp_

#include <bg/bg2e.hpp>

class MainEventHandler : public bg::base::EventHandler {
public:
	MainEventHandler();
	
	void willCreateContext();
	
	void initGL();
	void reshape(int,int);
	void draw();

protected:
	virtual ~MainEventHandler();
	
	bg::ptr<bg::render::Renderer> _renderer;
	bg::ptr<bg::scene::Node> _sceneRoot;
	bg::ptr<bg::scene::Camera> _camera;
};

#endif
