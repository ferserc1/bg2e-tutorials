
#include <event_handler.hpp>

MainEventHandler::MainEventHandler()
{
	
}

MainEventHandler::~MainEventHandler() {
	
}

void MainEventHandler::willCreateContext() {
	if (bg::engine::OpenGLCore::Supported()) {
		bg::Engine::Init(new bg::engine::OpenGLCore());
	}
	else {
		throw bg::base::CompatibilityException("No such suitable rendering engine.");
	}
}

void MainEventHandler::initGL() {
	bg::Engine::Get()->initialize(context());
	_renderer = bg::render::Renderer::Create(context(), bg::render::Renderer::kRenderPathForward);

	{
		using namespace bg::scene;
		using namespace bg::math;
		_sceneRoot = new Node(context());

		Node * cameraNode = new Node(context(), "sceneRoot");
		_sceneRoot->addChild(cameraNode);
		_camera = new Camera();
		cameraNode->addComponent(_camera.getPtr());
		cameraNode->addComponent(new Transform());
		bg::manipulation::OrbitNodeController * orbit = new bg::manipulation::OrbitNodeController;
		cameraNode->addComponent(orbit);
		bg::ptr<OpticalProjectionStrategy> projection = new OpticalProjectionStrategy;
		projection->setFocalLength(Scalar(35,distance::mm));
		_camera->setProjectionStrategy(projection.getPtr());
		
		orbit->setDistance(Scalar(5,distance::meter));
		orbit->setRotation(Vector2(Scalar(-22.5,trigonometry::deg),
								   Scalar( 45.0f,trigonometry::deg)));

		Node * lightNode = new Node(context());
		_sceneRoot->addChild(lightNode);
		Transform * trx = new Transform();
		trx->matrix()
			.rotate(Scalar(95,trigonometry::deg), -1.0f, 0.0f, 0.0f)
			.rotate(Scalar(45,trigonometry::deg), 0.0f, 1.0f, 0.0f);
		lightNode->addComponent(trx);
		lightNode->addComponent(new bg::scene::Light);

		PrimitiveFactory factory(context());
		Node * floor = new Node(context());
		_sceneRoot->addChild(floor);
		floor->addComponent(factory.plane(Scalar(5,distance::meter)));
		trx = new Transform();
		trx->matrix()
			.translate(0.0f, Scalar(-55,distance::cm), 0.0f);
		floor->addComponent(trx);

		Node * cube = new Node(context());
		trx = new Transform();
		trx->matrix()
			.rotate(trigonometry::degreesToRadians(22.5f), 0.0f, 1.0f, 0.0f);
		cube->addComponent(trx);
		_sceneRoot->addChild(cube);
		cube->addComponent(factory.cube(Scalar(1,distance::meter)));
		
		_inputVisitor = new bg::scene::InputVisitor;
	}
}

void MainEventHandler::willDestoryContext() {
	
}

void MainEventHandler::destroy() {
	
}

void MainEventHandler::reshape(int w, int h) {
	_camera->setViewport(bg::math::Viewport(0, 0, w, h));
}

void MainEventHandler::frame(float delta) {
	_renderer->frame(_sceneRoot.getPtr(), delta);
}

void MainEventHandler::draw() {
	_renderer->draw(_sceneRoot.getPtr(), _camera.getPtr());
	context()->swapBuffers();
}

void MainEventHandler::keyUp(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::keyDown(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->keyDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::charPress(const bg::base::KeyboardEvent & evt) {
	_inputVisitor->charPress(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDown(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDown(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseDrag(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseDrag(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseMove(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseUp(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseUp(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::mouseWheel(const bg::base::MouseEvent & evt) {
	_inputVisitor->mouseWheel(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::onMemoryWarning() {
}

void MainEventHandler::touchStart(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchStart(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchMove(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchMove(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::touchEnd(const bg::base::TouchEvent & evt) {
	_inputVisitor->touchEnd(_sceneRoot.getPtr(), evt);
}

void MainEventHandler::sensorEvent(const bg::base::SensorEvent & evt) {
	_inputVisitor->sensorEvent(_sceneRoot.getPtr(), evt);
}
